# Makefile just for this assignment
FILE=work

all: clean pdf

clean:
	if [ -a work.pdf ];\
	then\
		rm work.pdf;\
	fi;

pdf: $(FILE).pdf

$(FILE).pdf:
	latex $(FILE);
	bibtex $(FILE);
	latex $(FILE);
	pdflatex $(FILE);

